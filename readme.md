# Bash FreeTube-Android Notifier
A very simple bash script which sends subscription notifications to your phone via ntfy.sh, by watching the rss feeds for your subscriptions from a FreeTube `profiles.db` file.

## Usage:
1. Ensure you have the script's dependencies installed:
   - rsstail
   - inotify-tools
   - jq
   - curl
2. Put whatever you want your ntfy.sh topic to be in the third line of the bash script.
3. Setup ntfy on your phone to watch that topic.
4. Run the script in the same directory as your `profiles.db` file.

I recommend syncing your `profiles.db` file with whatever server you're using with [Syncthing](https://syncthing.net/). The script will watch the file for changes, and reload whenever they occur.

Or I guess you could run it from your phone using Termux or a VM or something, I'm not your mom.