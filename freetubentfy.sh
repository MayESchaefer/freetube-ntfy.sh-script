#!/bin/bash

ntfy_id="YOUR NTFY TOPIC"
pids_file="./freetubentfy_pids"

function tailfeed()
{
	{
		channel_thumbnail="$2"
		channel_id="$1"
		rsstail -i 60 -u "https://www.youtube.com/feeds/videos.xml?channel_id=$channel_id" -n 0 -l -a -d -H | while read line1
		do
			read line2
			read line3
			read line4
			nl=$'\n'
			line="$line1$nl$line2$nl$line3$nl$line4"
			title=$(echo "$line" | sed -rn 's/^Title: (.+)$/\1/p')
			author=$(echo "$line" | sed -rn 's/^Author: (.+)$/\1/p')
			link=$(echo "$line" | sed -rn 's/^Link: (.+)$/\1/p')
			curl "ntfy.sh/$ntfy_id" \
				-H "Title: $author" \
				-H "Click: $link" \
				-H "Icon: $channel_thumbnail" \
				-d "$title"
		done
	} > /dev/null
}

pids=()

function handlefile()
{
	echo "reading profiles..."

	echo "removing ${#pids[@]} old subprocesses."
	for id in "${pids[@]}"
	do
		(kill "$id") > /dev/null
	done

	pids=()
	echo "" > "$pids_file"

	echo "reading subscriptions..."
	while read p; do
		echo "reading line"

		echo $(echo "$p" | jq -c ".subscriptions") | jq -c ".[]" | while read i; do
			tailfeed $(echo "$i" | jq -r -c ".id") $(echo "$i" | jq -r -c ".thumbnail") &
			echo "$!" >> "$pids_file"
		done

		echo "done reading line"
	done < profiles.db

	readarray -t pids < "$pids_file"
	unset 'pids[0]'
	echo "" > "$pids_file"

	echo "tracking ${#pids[@]} subscriptions."
}

handlefile

echo "watching for changes..."
inotifywait -qme close_write profiles.db |
while read -r filename event; do
	handlefile
done